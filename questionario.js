function aleatorio(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}

function mistura(array) {
  let atual = array.length;
	let tmp, idx;

	while (0 !== atual) {
		idx = Math.floor(Math.random() * atual);
		atual -= 1;

		tmp = array[atual];
		array[atual] = array[idx];
		array[idx] = tmp;
	}

	return array;
};

function correto() {
  alert("Certa a resposta!");
}

function incorreto() {
  alert("Resposta incorreta, tente novamente");
}

function geraRadio(id, i, ehCorreta) {
  let metodo = ehCorreta ? "correto()" : "incorreto()";
  return `<input type="radio" value="${id}_${i}" name="${id}" onclick="${metodo}">`;
}

function geraQuestaoA(id) {
  // gera dois operandos aleatoriamente
  let numA = aleatorio(1, 10);
  let numB = aleatorio(1, 10);
  let res = numA + numB;

  // monta o enunciado
  let html = `<h2>Qual é o resultado da expressão ${numA} + ${numB}?</h2>`;
  html += "<form><fieldset>";
  html += '<ol type="a">';

  // monta as alternativas
  let alternativas = [];
  for(let i = -2; i <= 2; i++) {
    alternativas.push(res + i);
  }
  mistura(alternativas);

  for(let i = 0; i < alternativas.length; i++) {
    let ehCorreta = alternativas[i] == res;
    html += `<li>${geraRadio(id, i, ehCorreta)} ${alternativas[i]}</li>`;
  }

  html += "</form></fieldset>";
  html += '</ol>';

  $("#" + id).html(html);
}